# Setup

1. Fork this repo and give maintainer access to `Pankaj-Baranwal`. Clone your fork locally.
2. Create a new branch with this format: firstname-dev. Example: `pankaj-dev`
3. `pip install --upgrade openai`
4. Create a copy of `template/` folder and rename it to a self-explanatory project name.
5. Create a `.gitignore` file inside this newly created folder and add `settings.py` in it.
